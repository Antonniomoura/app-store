import {INavData} from './shared/models/nav-data';

export const navItems: INavData[] = [
  {
    title: true,
    name: 'Menu'
  },
  {
    name: 'Gastos',
    url: '/spending',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    name: 'Creditos',
    url: '/credits',
    icon: 'icon-drop'
  }
];
