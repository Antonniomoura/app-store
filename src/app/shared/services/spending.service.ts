import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {ISpending} from '../models/spending';

@Injectable({
  providedIn: 'root'
})
export class SpendingService {
  constructor(private http: HttpClient) {
  }

  fetchSpending(): Observable<ISpending[]> {
    return this.http.get<any>(`${environment.url}expenses`).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  fetchSpendingId(id: string): Observable<ISpending[]> {
    return this.http.get<any>(`${environment.url}expenses/${id}`).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  fetchByMothYear(year: number, mouth: number, idUser: string): Observable<ISpending[]> {
    return this.http.post<any>(`${environment.url}expenses/years`, {mouth, year, idUser}).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  fetchCode(code: string): Observable<ISpending[]> {
    return this.http.get<any>(`${environment.url}expenses/code/${code}`).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  deleteItem(id: string): Observable<ISpending[]> {
    return this.http.delete<any>(`${environment.url}expenses/${id}`).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  createSpending(spending: ISpending): Observable<any> {
    return this.http.post<any>(`${environment.url}expenses`, spending).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  updateSpending(id: string, spending: ISpending): Observable<any> {
    return this.http.put<any>(`${environment.url}expenses/${id}`, spending).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  handleError(error: HttpErrorResponse) {
    let message = '';

    if (error.error instanceof ErrorEvent) {
      message = `An error ocurred: ${error.statusText}`;
    } else {
      message = `Server returned code: ${error.status}. Error body is: ${error.error}`;
    }

    return throwError(message);
  }
}
