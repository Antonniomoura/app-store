import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {ISpending} from '../models/spending';
import {ICategory} from '../models/category';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) {
  }

  fetchCategories(): Observable<ICategory[]> {
    return this.http.get<any>(`${environment.url}categories`).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  fetchCategoriesSlug(slug: string): Observable<ISpending[]> {
    return this.http.get<any>(`${environment.url}expenses/slug/${slug}`).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  fetchCategoriesById(id: string): Observable<ICategory> {
    return this.http.get<any>(`${environment.url}categories/${id}`).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  deleteItem(id: string): Observable<ICategory[]> {
    return this.http.delete<any>(`${environment.url}categories/${id}`).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  createCategories(categories: ICategory): Observable<any> {
    return this.http.post<any>(`${environment.url}categories`, categories).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  updateCategories(id: string, categories: ICategory): Observable<any> {
    return this.http.put<any>(`${environment.url}categories/${id}`, categories).pipe(
      map(userProfile => userProfile),
      catchError(this.handleError),
    );
  }

  handleError(error: HttpErrorResponse) {
    let message = '';

    if (error.error instanceof ErrorEvent) {
      message = `An error ocurred: ${error.statusText}`;
    } else {
      message = `Server returned code: ${error.status}. Error body is: ${error.error}`;
    }

    return throwError(message);
  }
}
