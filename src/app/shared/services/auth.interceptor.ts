import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = JSON.parse(localStorage.getItem('token'));

    if (token) {
      const authReq = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token.token}`
        }
      });
      return next.handle(authReq);
    }
    return next.handle(req);
  }
}
