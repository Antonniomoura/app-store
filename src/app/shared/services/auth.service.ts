import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {catchError, map, tap} from 'rxjs/operators';
import {BehaviorSubject, Observable, of, pipe, throwError} from 'rxjs';
import {IUser} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseHeaders = {'Content-Type': 'application/json'};
  private headers = new HttpHeaders(this.baseHeaders);
  private subjectUser$: BehaviorSubject<IUser> = new BehaviorSubject(null);
  private subjectLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private router: Router, private http: HttpClient) {
  }

  login(email: string, password: string): Observable<IUser> {
    return this.http.post<any>(`${environment.url}api/login`, {user: {email, password}}, {headers: this.headers}).pipe(
      pipe(
        tap((user) => {
          localStorage.setItem('token', JSON.stringify(user));
          this.subjectLoggedIn$.next(true);
          this.subjectUser$.next(user);
        })
      ),
    );
  }

  logout() {
    localStorage.removeItem('token');
    this.subjectUser$.next(null);
    this.subjectLoggedIn$.next(false);
  }

  register(user: IUser) {
    return this.http.post<IUser>(`${environment.url}api/login`, user);
  }

  getUser(): Observable<IUser> {
    return this.subjectUser$.asObservable();
  }

  validationToken(): Observable<boolean> {
    return this.http.get<IUser>(`${environment.url}api/me`).pipe(
      tap((user) => {
        if (user) {
          this.subjectUser$.next(user);
          this.subjectLoggedIn$.next(true);
        }
      }),
      map((user) => !!user),
      catchError((err) => {
        this.logout();
        return of(false);
      })
    );
  }

  isAuthenticated(): Observable<boolean> {
    const token = JSON.parse(localStorage.getItem('token'));
    if (token && token.token && !this.subjectUser$.value) {
      return this.validationToken();
    }
    return this.subjectLoggedIn$.asObservable();
  }

  handleError(error: HttpErrorResponse) {
    let message = '';

    if (error.error instanceof ErrorEvent) {
      message = `An error ocurred: ${error.statusText}`;
    } else {
      message = `Server returned code: ${error.status}. Error body is: ${error.error}`;
    }

    return throwError(message);
  }
}
