export interface ISpendingComponent {
  total: number;
  menorGasto: number;
  periodo: string;
  maiorGasto: number;
  pago: boolean;
  acoes: string;
  codIdentifier: string;
}
