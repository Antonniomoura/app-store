export interface ICategory {
  _id?: string;
  status: boolean | string;
  idUser: string;
  name: string;
  slug: string;
  description?: string;
}
