export interface IUser {
  fistName: string;
  lastName: string;
  imgUrl?: string;
  password: string;
  email: string;
  code?: string;
}
