export interface ISpending {
  _id: string;
  amount: number;
  category: string;
  mouth: number;
  description: string;
  year: number;
  idUser: string;
  status: boolean;
  debt: boolean;
  date: string;
  codIdentifier: string;
}
