import { Routes } from '@angular/router';

// Route for content layout with sidebar, navbar and footer.

export const fullRoutes: Routes = [
  {
    path: '',
    loadChildren: './spending/spending.module#SpendingModule',
  },
];
