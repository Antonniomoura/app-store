import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SpendingComponent} from './spending/spending.component';
import {SpendingFormComponent} from './spending-form/spending-form.component';
import {SpendingDetailsComponent} from './spending-details/spending-details.component';
import {CreditsComponent} from './credits/credits.component';


const routes: Routes = [
  {path: 'credits', component: CreditsComponent, data: {title: 'Credito'}},
  {path: 'credits/form', component: SpendingFormComponent, data: {title: 'Novo Credito'}},
  {path: 'credits/:code/details', component: SpendingDetailsComponent, data: {title: 'Detalhes Creditos'}},
  {path: 'spending', component: SpendingComponent, data: {title: 'Gastos'}},
  {path: 'spending/form', component: SpendingFormComponent, data: {title: 'Novo Gasto'}},
  {path: 'spending/:code/details', component: SpendingDetailsComponent, data: {title: 'Detalhes Gastos'}},
  {path: 'spending/form/:id', component: SpendingFormComponent, data: {title: 'Editar Gastos'}},
  {path: '', pathMatch: 'full', redirectTo: 'spending', data: {title: 'Gastos'}},
  {path: '**', redirectTo: 'spending', data: {title: 'Gastos'}},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpendingRoutingModule {
}
