import {LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SpendingRoutingModule} from './spending-routing.module';
import {SpendingComponent} from './spending/spending.component';
import {SpendingFormComponent} from './spending-form/spending-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SpendingDetailsComponent } from './spending-details/spending-details.component';
import { CreditsComponent } from './credits/credits.component';


@NgModule({
  declarations: [SpendingComponent, SpendingFormComponent, SpendingDetailsComponent, CreditsComponent],
  imports: [
    CommonModule,
    FormsModule,
    SpendingRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [
  ],
})
export class SpendingModule {
}
