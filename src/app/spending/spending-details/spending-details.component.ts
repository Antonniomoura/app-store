import {Component, OnInit} from '@angular/core';
import * as moment from 'moment';
import {ActivatedRoute, Router} from '@angular/router';
import {SpendingService} from '../../shared/services/spending.service';
import {ISpending} from '../../shared/models/spending';
import {ICategory} from '../../shared/models/category';
import {AuthService} from '../../shared/services/auth.service';
import {CategoriesService} from '../../shared/services/categories.service';

@Component({
  selector: 'app-spending-details',
  templateUrl: './spending-details.component.html',
  styleUrls: ['./spending-details.component.scss']
})
export class SpendingDetailsComponent implements OnInit {
  spreddings: ISpending[];
  date: any;
  categories: ICategory[];
  isCredit = false;
  percentage = 0;
  total = 0;
  valuePaid = 0;

  constructor(
    private spendingService: SpendingService,
    private categoriesService: CategoriesService,
    private router: Router,
    private authService: AuthService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.getItems();
  }

  dataNow() {
    return this.date = moment(new Date()).format('DD MMMM YYYY');
  }

  totalPaid(spendings: any) {
    const reducer = (accumulator: number, currentValue: number) => accumulator + currentValue;
    this.total = spendings.map((spen: ISpending) => spen.amount).reduce(reducer);
    const valor: Array<any> = spendings.filter(spen => !spen.status);
    this.valuePaid = valor.length ? valor.map((spen: ISpending) => spen.amount).reduce(reducer) : 0;
    this.percentage = Math.round((this.valuePaid / this.total) * 100);
  }

  getItems() {
    this.route.params.subscribe(params => {
      this.authService.getUser().subscribe(suce => {
        this.spendingService.fetchCode(params.code).subscribe(sucess => {
          this.route.url.subscribe(suces => {
            this.isCredit = sucess ? suces[0].path === 'credits' : false;
            if (sucess.length === 0) {
              this.router.navigateByUrl(this.isCredit ? '/credits' : '/spending');
            }

            const newDate = new Date(params.code.slice(0, 2) + '/02/' + params.code.slice(2, 6));
            this.date = moment(newDate).format('MMMM YYYY');
            this.spreddings = sucess.filter(ele => ele.idUser === suce.code);

            if (this.isCredit) {
              this.spreddings = this.spreddings.filter(spendding => !spendding.debt);
            } else {
              this.spreddings = this.spreddings.filter(spendding => spendding.debt);
            }
            this.totalPaid(this.spreddings);
          });

        });
        this.categoriesService.fetchCategories().subscribe(success => {
          this.categories = success.filter(ele => ele.idUser === suce.code);
        });
      });
    });
  }

  nameCategory(id: string): string {
    const catgoryName: ICategory[] | [] = this.categories ? this.categories.filter(ele => ele._id === id) : [];
    if (catgoryName && catgoryName[0] && catgoryName[0].name) {
      return catgoryName[0].name;
    }

    return id;
  }

  changeStatus(spredding: ISpending) {
    spredding.status = !spredding.status;
    this.spendingService.updateSpending(spredding._id, spredding).subscribe(() => {
      this.getItems();
    });
  }

  clone(clone: any) {
    delete clone._id;
    this.spendingService.createSpending(clone).subscribe(() => {
      this.getItems();
    }, () => {
      console.log('error');
    });
  }

  formatDate(date: string) {
    return moment(date).format('DD/MM/YY');
  }

  formatCategory(category: string) {
    return category.replace('_', ' ').toLocaleUpperCase();
  }


  navigateByUrl(id: string) {
    this.router.navigateByUrl('/spending/form/' + id);
  }

  delete(id: string) {
    this.spendingService.deleteItem(id).subscribe(() => {
      this.getItems();
    });
  }
}
