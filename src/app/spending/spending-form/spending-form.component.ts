import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {SpendingService} from '../../shared/services/spending.service';
import * as moment from 'moment';
import {AuthService} from '../../shared/services/auth.service';
import {CategoriesService} from '../../shared/services/categories.service';
import {ICategory} from '../../shared/models/category';

@Component({
  selector: 'app-spending-form',
  templateUrl: './spending-form.component.html',
  styleUrls: ['./spending-form.component.scss']
})
export class SpendingFormComponent implements OnInit {
  contactForm: FormGroup;
  disabledSubmitButton = true;
  categories: ICategory[];
  loading = true;
  isCredit = false;
  id: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private categoriesService: CategoriesService,
    private spendingService: SpendingService
  ) {

    this.authService.getUser().subscribe(user => {
      this.route.url.subscribe(sucess => {
        this.isCredit = sucess ? sucess[0].path === 'credits' : false;
        this.contactForm = this.fb.group({
          amount: [0, Validators.required],
          category: ['outra'],
          codIdentifier: [''],
          description: [''],
          date: ['', Validators.required],
          debt: [!this.isCredit],
          idUser: [user.code],
          status: [true]
        });
        this.loading = false;
      });
      this.getItems();
    });
  }

  getItems() {
    this.categoriesService.fetchCategories().subscribe(categories => {
      this.categories = categories;
    });
    this.route.params.subscribe((params: any) => {
      if (params.id) {
        this.id = params.id;
        this.spendingService.fetchSpendingId(params.id).subscribe((success: any) => {
          this.contactForm.controls.amount.setValue(success.amount);
          this.contactForm.controls.category.setValue(success.category);
          this.contactForm.controls.date.setValue(success.date);
          this.contactForm.controls.debt.setValue(success.debt ? '1' : '2');
          this.contactForm.controls.description.setValue(success.description);
          this.contactForm.controls.idUser.setValue(success.idUser);
          this.contactForm.controls.codIdentifier.setValue(success.codIdentifier);
          this.contactForm.controls.status.setValue(success.status);
        });
      }
    });
  }

  onSubmit() {
    const spending: any = this.contactForm.value;
    spending.status = !!(spending.debt && spending.status);
    if (!this.id) {
      this.spendingService.createSpending(spending).subscribe(() => {
        this.contactForm.reset();
        this.router.navigateByUrl(this.isCredit ? '/credits' : '/spending');
        this.disabledSubmitButton = true;
      }, () => {
        console.log('Error');
      });
    } else {
      spending.codIdentifier = this.generatorCode(spending);
      this.spendingService.updateSpending(this.id, spending).subscribe(() => {
        this.contactForm.reset();
        this.router.navigateByUrl(`/${this.isCredit ? 'credits' : 'spending'}/${spending.codIdentifier}/details`);
        this.disabledSubmitButton = true;
      }, () => {
        console.log('Error');
      });
    }
  }

  generatorCode(createdExpense: any) {
    if (createdExpense && createdExpense.date) {
      moment.locale('pt-br');
      let newCode = moment(new Date(createdExpense.date)).format('L');
      if (!newCode) {
        return 'error';
      }
      newCode = newCode.slice(3);
      newCode = newCode.replace('/', '');
      return newCode;
    }
  }

  ngOnInit(): void {
  }

}
