import {Component, OnInit} from '@angular/core';
import {SpendingService} from '../../shared/services/spending.service';
import * as moment from 'moment';
import {SpendingComponentInterface} from '../spending/spending.component';
import {ISpending} from '../../shared/models/spending';
import {AuthService} from '../../shared/services/auth.service';

@Component({
  selector: 'app-credits',
  templateUrl: './credits.component.html',
  styleUrls: ['./credits.component.scss']
})
export class CreditsComponent implements OnInit {
  ELEMENT_DATA: SpendingComponentInterface[] = [];
  displayedColumns: string[] = ['periodo', 'maiorGasto', 'menorGasto', 'total', 'pago', 'acoes'];
  dataSource;
  spendings: ISpending[];
  spendingToRender: [{ codIdentifier: string, spending: ISpending[] }];

  constructor(private spendingService: SpendingService, private authService: AuthService) {
  }

  ngOnInit() {
    this.spendingService.fetchSpending().subscribe((success: any) => {
      const codIdentifier: Array<string> = [];
      this.spendings = success.sort((actual: any, next: any) => (actual.amount > next.amount ? 1 : -1) || 0);
      this.spendings = this.spendings.filter(spendding => !spendding.debt);
      if (this.spendings) {
        this.authService.getUser().subscribe(suce => {
          this.spendings.forEach((spending) => {
            if (spending.idUser === suce.code) {
              if (!this.spendingToRender) {
                this.spendingToRender = [{codIdentifier: spending.codIdentifier, spending: [spending]}];
                codIdentifier.push(spending.codIdentifier);
              } else {
                if (codIdentifier.find(element => element === spending.codIdentifier)) {
                  // @ts-ignore
                  this.spendingToRender = this.spendingToRender.map(element => {
                    if (element.codIdentifier === spending.codIdentifier) {
                      element.spending.push(spending);
                    }
                    return element;
                  });
                } else {
                  codIdentifier.push(spending.codIdentifier);
                  this.spendingToRender.push({codIdentifier: spending.codIdentifier, spending: [spending]});
                  codIdentifier.push(spending.codIdentifier);
                }
              }
            }
          });
        });
      }
      this.ELEMENT_DATA = this.convertArray(this.spendingToRender);
      this.dataSource = this.ELEMENT_DATA;
    });
  }

  convertArray(spendingToRender: [{ codIdentifier: string, spending: ISpending[] }]): SpendingComponentInterface[] {
    const newElementData: SpendingComponentInterface[] = [];
    let periodo: string, maiorGasto: number, menorGasto: number, total: number, pago: boolean;
    if (!spendingToRender || spendingToRender.length <= 0) {
      return [];
    }
    spendingToRender.forEach(spending => {
      total = this.totalPayment(spending.spending);
      menorGasto = this.lowestPayment(spending);
      pago = !!this.spendingStatus(spending.spending);
      maiorGasto = this.highestPayment(spending);
      periodo = this.returnDate(spending.codIdentifier);
      newElementData.push({maiorGasto, menorGasto, pago, periodo, total, acoes: '', codIdentifier: spending.codIdentifier});
    });
    return newElementData;
  }

  highestPayment(spendings: { spending: ISpending[] }) {
    const spending: ISpending = spendings.spending.sort((actual, next) => (actual.amount < next.amount ? 1 : -1) || 0)[0];
    return spending.amount;
  }

  lowestPayment(spendings: { spending: ISpending[] }) {
    const spending: ISpending = spendings.spending.sort((actual, next) => (actual.amount > next.amount ? 1 : -1) || 0)[0];
    return spending.amount;
  }

  totalPayment(spendings: any) {
    const reducer = (accumulator: number, currentValue: number) => accumulator + currentValue;
    return spendings.map((spen: ISpending) => spen.amount).reduce(reducer);
  }

  dateClosed(date: ISpending[]) {
    let closed = true;
    date.forEach((mon: ISpending) => {
      if (mon.status) {
        closed = false;
      }
    });
    return closed;
  }

  spendingStatus(data: ISpending[]): ISpending | undefined {
    return data.find((spending: ISpending) => spending.status);
  }

  returnDate(date: string) {
    moment.locale('pt-br');
    const newDate = date.slice(0, 2) + '/02/' + date.slice(2, 6);
    return moment(new Date(newDate)).format('MMMM - YYYY');
  }

}
