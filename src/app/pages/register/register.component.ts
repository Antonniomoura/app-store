import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IUser} from '../../shared/models/user';
import {AuthService} from '../../shared/services/auth.service';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {
  loading: boolean = false;
  formRegister: FormGroup = this.fb.group({
    fistName: ['', [Validators.required]],
    lastName: [''],
    email: ['', [Validators.required]],
    password1: ['', [Validators.required]],
    password2: ['', [Validators.required]],
  }, {
    validators: this.matchingPassword
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    this.authService.isAuthenticated().subscribe(login => {
      this.loading = true;
      this.router.navigateByUrl('/');
    });
  }

  matchingPassword(group: FormGroup) {
    if (group && group.controls && group.controls.password) {
      const password1 = group.controls.password.value;
      const password2 = group.controls.password.value;
      if (password1 === password2) {
        return null;
      }
      return {matching: false};
    }
  }

  onSubmit() {
    const newUser: IUser = {
      fistName: this.formRegister.value.firstName,
      lastName: this.formRegister.value.lastName,
      email: this.formRegister.value.email,
      password: this.formRegister.value.password,
    };
  }

}
